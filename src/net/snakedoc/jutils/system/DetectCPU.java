/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.system;

/**
 * Detect CPU info
 * 
 * @author Jason Sipula
 *
 */
public class DetectCPU {
	
	/**
	 * Get number of cores available to JVM (usually number on machine).
	 * 
	 * @return integer value of number of cpu cores availabe.
	 */
	public static int getCores() {
		return Runtime.getRuntime().availableProcessors();
	}
	
	/**
	 * Get CPU architecture.
	 * 
	 * @return String value of the cpu's architecture.
	 */
	public static String getCPUArch() {
		return System.getProperty("os.arch");
	}
}
