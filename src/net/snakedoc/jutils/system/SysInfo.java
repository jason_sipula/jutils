/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.system;

import java.io.File;

/**
 * Detection of environment utilities.
 * 
 * @author Jason Sipula
 *
 */
public class SysInfo {
	
	/**
	 * Get number of Cores available to JVM (usually number of cores on machine).
	 * 
	 * @return integer value of number of cores available to JVM (usually number of cores on machine).
	 */
	public int getCores() {
		return Runtime.getRuntime().availableProcessors();
	}
	
	/**
	 * Get CPU Architecture (32 bit / 64 bit, etc...)
	 * 
	 * @return String value of CPU Architecture.
	 */
	public String getCPUArch() {
		return System.getProperty("os.arch");
	}
	
	/**
	 * Get Total Memory available to JVM.
	 * 
	 * @return Numeric (long) value in KB.
	 */
	public long getTotalMem() {
		return (Runtime.getRuntime().totalMemory() );/// 1048576);
	}
	
	/**
	 * Maximum memory used by JVM during runtime in KB.
	 * 
	 * @return Numeric (long) value in KB.
	 */
	public long getMaxMem() {
		return (Runtime.getRuntime().maxMemory() );/// 1048576);
	}
	
	/**
	 * Get free memory.
	 * 
	 * @return Numeric (long) value of free memory in KB.
	 */
	public long getFreeMem() {
		return (Runtime.getRuntime().freeMemory() );/// 1048576);
	}
	
	/**
	 * Get array of currently mounted drive root directories (ex: / on linux and C:\ on windows).
	 * 
	 * @return Array of files (array in the event that multiple are returned for multiple mounted drives).
	 */
	public File[] getRoot() {
		return File.listRoots();
	}
	
	/**
	 * Detect Operating System Utilities
	 */
	
	/**
     * Get Operating System and return
     * String:<br>
     *     Windows - if Windows variant OS.<br>
     *     Unix/Linux - if Unix/Linux variant OS.<br>
     *     Mac - if Mac variant OS.<br>
     *     Solaris - if Solaris variant OS.<br>
     *     Unsupported OS - if Unknown type.
     * 
     * @param direct Use "Direct" method and change
     *          return to raw string output instead of
     *          standardized library return.
     * @return String:<br>
     *     Windows - if Windows variant OS.<br>
     *     Unix/Linux - if Unix/Linux variant OS.<br>
     *     Mac - if Mac variant OS.<br>
     *     Solaris - if Solaris variant OS.<br>
     *     Unsupported OS - if Unknown type.<br>
     *     OR<br>
     *     if "direct" param is set to true, then
     *     return raw string output.
     */
    public String getOS(boolean direct) {
        String os = System.getProperty("os.name");
        if (isWindows(os.toLowerCase())) {
            if (direct) {
                return os;
            } else {
                return "Windows";
            }
        } else if (isUnix(os.toLowerCase())) {
            if (direct) {
                return os;
            } else {
                return "Unix/Linux";
            }
        } else if (isMac(os.toLowerCase())) {
            if (direct) {
                return os;
            } else {
                return "Mac";
            }
        } else if (isSolaris(os.toLowerCase())) {
            if (direct) {
                return os;
            } else {
                return "Solaris";
            }
        }
        return "Unsupported OS";
    }
    private boolean isWindows(String os) {
        return (os.indexOf("win") > -1);
    }
    private boolean isUnix(String os) {
        return ((os.indexOf("nux") > -1) || (os.indexOf("nix") > -1));
    }
    private boolean isMac(String os) {
        return (os.indexOf("mac") > -1);
    }
    private boolean isSolaris(String os) {
        return ((os.indexOf("sunos") > -1) || (os.indexOf("solaris") > -1));
    }
    
    /**
     * Get OS Architecture. (32 bit / 64 bit, etc...)
     * 
     * @return String value of OS Architecture.
     */
    public String getOSArch() {
        return System.getProperty("sun.arch.data.model");
    }
}
