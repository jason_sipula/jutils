/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.system;

/**
 * Detect Operating System Utilities
 * 
 * @author Jason Sipula
 * 
 * Deprecated: Class being merged into
 *              SysInfo class. Will be
 *              removed two releases after
 *              current stable version 0.1.0
 *
 */
@Deprecated
public class DetectOS {
	
	/**
	 * Get Operating System and return
	 * String:<br>
	 *     Windows - if Windows variant OS.<br>
	 *     Unix/Linux - if Unix/Linux variant OS.<br>
	 *     Mac - if Mac variant OS.<br>
	 *     Solaris - if Solaris variant OS.<br>
	 *     Unsupported OS - if Unknown type.
	 * 
	 * @param direct Use "Direct" method and change
	 * 			return to raw string output instead of
	 * 			standardized library return.
	 * @return String:<br>
	 *     Windows - if Windows variant OS.<br>
	 *     Unix/Linux - if Unix/Linux variant OS.<br>
	 *     Mac - if Mac variant OS.<br>
	 *     Solaris - if Solaris variant OS.<br>
	 *     Unsupported OS - if Unknown type.<br>
	 *     OR<br>
	 *     if "direct" param is set to true, then
	 *     return raw string output.
	 */
	public static String getOS(boolean direct) {
		String os = System.getProperty("os.name");
		if (isWindows(os.toLowerCase())) {
			if (direct) {
				return os;
			} else {
				return "Windows";
			}
		} else if (isUnix(os.toLowerCase())) {
			if (direct) {
				return os;
			} else {
				return "Unix/Linux";
			}
		} else if (isMac(os.toLowerCase())) {
			if (direct) {
				return os;
			} else {
				return "Mac";
			}
		} else if (isSolaris(os.toLowerCase())) {
			if (direct) {
				return os;
			} else {
				return "Solaris";
			}
		}
		return "Unsupported OS";
	}
	private static boolean isWindows(String os) {
		return (os.indexOf("win") > -1);
	}
	private static boolean isUnix(String os) {
		return ((os.indexOf("nux") > -1) || (os.indexOf("nix") > -1));
	}
	private static boolean isMac(String os) {
		return (os.indexOf("mac") > -1);
	}
	private static boolean isSolaris(String os) {
		return ((os.indexOf("sunos") > -1) || (os.indexOf("solaris") > -1));
	}
	
	/**
	 * Get OS Architecture. (32 bit / 64 bit, etc...)
	 * 
	 * @return String value of OS Architecture.
	 */
	public static String getOSArch() {
		return System.getProperty("sun.arch.data.model");
	}
}
