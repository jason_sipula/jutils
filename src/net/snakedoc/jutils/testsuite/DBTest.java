package net.snakedoc.jutils.testsuite;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import net.snakedoc.jutils.Config;
import net.snakedoc.jutils.ConfigException;
import net.snakedoc.jutils.database.*;
import net.snakedoc.jutils.timer.MilliTimer;

/**
 * Database Testing Utilities.
 * 
 * @author Jason Sipula
 *
 */
public class DBTest {
	
	private static final Logger log = Logger.getLogger(DBTest.class);
	private static final Config config = new Config();
	
	final int INSERT_LIMIT = 1048576;

	/**
	 * H2 Database Test Script.<br>
	 * Iterates 1,048,576 records.
	 */
	public void h2Test() {
	    config.loadConfig("props/log4j.properties"); // load the log4j properties file
		H2 h2 = null;
		try {
			h2 = new H2();
		} catch (ConfigException e) {
            log.error("Error: ", e);
        }
		try {
			h2.openConnection();
		} catch (ClassNotFoundException e1) {
			log.error("Error: ", e1);
		} catch (SQLException e1) {
			log.error("Error: ", e1);
		}
		String sqlDrop = "DROP TABLE IF EXISTS sample";
		String sqlCreate = "CREATE TABLE sample" +
				"( samp1 VARCHAR(32) , samp2 VARCHAR(32) )";
		String sqlInsert = "INSERT INTO sample VALUES(?, ?)";
		String sqlSelect = "SELECT * FROM sample";
		try {
			MilliTimer timer = new MilliTimer();
			timer.startTimer();
			
			SecureRandom random = new SecureRandom();
			
			PreparedStatement psDrop = h2.getConnection().prepareStatement(sqlDrop);
			PreparedStatement psCreate = h2.getConnection().prepareStatement(sqlCreate);
			
			log.info("Droping table 'sample' if exists");
			if (!(psDrop.executeUpdate()  > 0)) {
				log.info("Dropped table");
			} else {
				log.info("Table not dropped");
			}
			
			log.info("Creating table 'sample'");
			psCreate.execute();
			log.info("Created table");
			
			PreparedStatement psInsert = h2.getConnection().prepareStatement(sqlInsert);
			PreparedStatement psSelect = h2.getConnection().prepareStatement(sqlSelect);
			
			log.info("Insert data to table 'sample'");
			MilliTimer looptimer = new MilliTimer();
			looptimer.startTimer();
			for (int i = 0; i < INSERT_LIMIT; i++) {
				psInsert.setString(1, new BigInteger(130, random).toString(32));
				psInsert.setString(2, new BigInteger(130, random).toString(32));
				psInsert.execute();
			}
			looptimer.stopTimer();
			log.info("Insert data complete");
			
			log.info("Select data from table 'sample'");
			MilliTimer rstimer = new MilliTimer();
			rstimer.startTimer();
			ResultSet rs = psSelect.executeQuery();
			rstimer.stopTimer();
			log.info("Selected data complete");
			
			log.info("Print from result set");
			MilliTimer ptimer = new MilliTimer();
			ptimer.startTimer();
			while(rs.next()) {
				System.out.println(rs.getString(1) + " | " + rs.getString(2));
			}
			ptimer.stopTimer();
			log.info("Print from result set complete");

			timer.stopTimer();
			
			log.info("##################################################");
			log.info("Total test time: " + timer.getTime());
			log.info("INSERT " + INSERT_LIMIT + " records in " 
					+ looptimer.getTime());
			log.info("Selected data in " + rstimer.getTime());
			log.info("Printed " + INSERT_LIMIT + " records in " + ptimer.getTime());
			
		} catch (SQLException e) {
			log.error("Error: ", e);
		}
		try {
			h2.closeConnection();
		} catch (SQLException e) {
			log.error("Error: ", e);
		}
	}
}
