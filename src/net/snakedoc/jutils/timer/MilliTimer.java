/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.timer;

/**
 * Timer and utils (using milliseconds)
 * 
 * @author Jason Sipula
 *
 */
public class MilliTimer {
	
	private long startTime = 0L;
	private long stopTime = 0L;
	
	/**
	 * Starts timer.
	 */
	public void startTimer() {
		startTime = System.nanoTime();
	}
	
	/**
	 * Stops timer.
	 */
	public void stopTimer() {
		stopTime = System.nanoTime();
	}
	
	/**
	 * Subtracts the input amount of milliseconds
	 * from the stopTime (which is obtained after
	 * calling stopTimer() method).
	 * 
	 * @param milliseconds Input amount of milliseconds to subtract.
	 */
	public void subtractTimer(long milliseconds) {
		stopTime -= milliseconds;
	}
	
	/**
	 * Adds the input amount of milliseconds
	 * from the stopTime (which is obtained after
	 * calling stopTimer() method).
	 * 
	 * @param milliseconds Input amount of milliseconds to add.
	 */
	public void addToTimer(long milliseconds) {
		stopTime += milliseconds;
	}
	
	/**
	 * Get Time in milliseconds (stopTime - startTime).
	 * 
	 * @return Floating point representation of time between stopTime and startTime.
	 */
	public float getMilliTime() {
		return ((stopTime - startTime) / 1000000000F);
	}
	
	/**
	 * Gets nicely formatted time represented as a string value as:<br>
	 *     x (m) - if x is represented in minutes.<br>
	 *     or<br>
	 *     x (s) - if x is represented in seconds.<br>
	 * 
	 * @return String value + (m) or (s).
	 */
	public String getTime() {
		String statement = "";
		float time = ((stopTime - startTime) / 1000000000F);
		if (time > 120.00f) {
			time = (time / 60.00f);
			statement += time + " (m)";
		} else {
			statement += time + " (s)";
		}
		return statement;
	}
}
