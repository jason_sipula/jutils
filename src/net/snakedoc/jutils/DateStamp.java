/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * DateStamp/TimeStamp Utilities.
 * 
 * @author Jason Sipula
 *
 */
public class DateStamp {
	
	/**
	 * Get DateStamp in specified format.
	 * 
	 * @param format Format of datestamp (Ex: yyyyMMdd_hhmmss)
	 * @param timeZone Timezone to get datestamp of.
	 * @return String value of datestamp.
	 */
	public String getDateStamp(String format, String timeZone) {
		
		String dateStamp = "";
		
		DateFormat df = new SimpleDateFormat(format);  
	    df.setTimeZone(TimeZone.getTimeZone(timeZone.toUpperCase()));  
	    dateStamp = df.format(new Date()); 
	    
		return dateStamp;
	}
}