/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.ee;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Send email message utilities.
 * 
 * @author Jason Sipula
 *
 */
public class SendEmail {
	
	/**
	 * Sends email message via Gmail service.
	 * 
	 * @param from Who the email is from. Must be a valid Gmail email account.
	 * 			It is recommened to have a separate, dedicated Gmail account setup
	 * 			for use here alone (for security reasons).
	 * @param pass Password for the Gmail email Account.
	 * @param subject Subject line of the email.
	 * @param body Body of the email.
	 * @param emailTo Recipient of the email. Must be a valid email address.
	 * @throws AddressException if Address is invalid.
	 * @throws MessagingException if message is invalid.
	 */
	public void sendGmail(String from, String pass, String subject, String body, String emailTo)
			throws AddressException, MessagingException {
		String host = "smpt.gmail.com";
		Properties props = System.getProperties();
		props.put("mail.smtp.starttls.enable", "true"); // added this line
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

	    String[] to = {emailTo}; // added this line

	    Session session = Session.getDefaultInstance(props, null);
	    MimeMessage message = new MimeMessage(session);
	    message.setFrom(new InternetAddress(from));

	    InternetAddress[] toAddress = new InternetAddress[to.length];

	    // To get the array of addresses
	    for( int i=0; i < to.length; i++ ) { // changed from a while loop
	        toAddress[i] = new InternetAddress(to[i]);
	    }
	    //System.out.println(Message.RecipientType.TO);

	    for( int i=0; i < toAddress.length; i++) { // changed from a while loop
	        message.addRecipient(Message.RecipientType.TO, toAddress[i]);
	    }
	    message.setSubject(subject);
	    message.setText(body);
	    System.out.println("\tTo: " + emailTo + "\n\tFrom: " + from + "" +
	    		"\n\tSubject: " + subject + "\n\tBody: " + body);
	    Transport transport = session.getTransport("smtp");
	    transport.connect(host, from, pass);
	    transport.sendMessage(message, message.getAllRecipients());
	    transport.close();
	}
}
