/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hashing utilities.
 * 
 * @author Jason Sipula
 *
 */
public class Hasher {
	
	/**
	 * Gets Hash of file using java.nio File Channels and ByteBuffer 
	 * <br/>for native system calls where possible. This may improve <br/>
	 * performance in some circumstances.
	 * 
	 * @param fileStr String path + filename of file to get hash.
	 * @param hashAlgo Hash algorithm to use. <br/>
	 *     Supported algorithms are: <br/>
	 *     MD2, MD5 <br/>
	 *     SHA-1 <br/>
	 *     SHA-256, SHA-384, SHA-512
	 * @param BUFFER Buffer size in bytes. Recommended to stay in<br/>
	 *          multiples of 2 such as 1024, 2048, <br/>
	 *          4096, 8192, 16384, 32768, 65536, etc.
	 * @return String value of hash. (Variable length dependent on hash algorithm used)
	 * @throws IOException If file is invalid.
	 * @throws HashTypeException If no supported or valid hash algorithm was found.
	 */
	public String getHashNIO(String fileStr, String hashAlgo, int BUFFER) throws IOException, HasherException {
		
		File file = new File(fileStr);
		
		MessageDigest md = null;
		FileInputStream fis = null;
		FileChannel fc = null;
		ByteBuffer bbf = null;
		StringBuilder hexString = null;
		
		try {
			md = MessageDigest.getInstance(hashAlgo);
			fis = new FileInputStream(file);
			fc = fis.getChannel();
			bbf = ByteBuffer.allocateDirect(BUFFER); // allocation in bytes - 1024, 2048, 4096, 8192
			
			int b;
			
			b = fc.read(bbf);
			
			while ((b != -1) && (b != 0)) {
			    bbf.flip();
			    
			    byte[] bytes = new byte[b];
			    bbf.get(bytes);
			    
			    md.update(bytes, 0, b);
			    
			    bbf.clear();
			    b = fc.read(bbf);
            }

			fis.close();
			
			byte[] mdbytes = md.digest();

			hexString = new StringBuilder();
			
			for (int i = 0; i < mdbytes.length; i++) {
				hexString.append(Integer.toHexString((0xFF & mdbytes[i])));
			}
			
			return hexString.toString();
		
		} catch (NoSuchAlgorithmException e) {
			throw new HasherException("Unsupported Hash Algorithm.", e);
		}
	}
	
	/**
     * Gets Hash of file.
     * 
     * @param file String path + filename of file to get hash.
     * @param hashAlgo Hash algorithm to use. <br/>
     *     Supported algorithms are: <br/>
     *     MD2, MD5 <br/>
     *     SHA-1 <br/>
     *     SHA-256, SHA-384, SHA-512
     * @param BUFFER Buffer size in bytes. Recommended to stay in<br/>
     *          multiples of 2 such as 1024, 2048, <br/>
     *          4096, 8192, 16384, 32768, 65536, etc.
     * @return String value of hash. (Variable length dependent on hash algorithm used)
     * @throws IOException If file is invalid.
     * @throws HashTypeException If no supported or valid hash algorithm was found.
     */
    public String getHash(String file, String hashAlgo, int BUFFER) throws IOException, HasherException {
        StringBuffer hexString = null;
        try {
            MessageDigest md = MessageDigest.getInstance(validateHashType(hashAlgo));
            FileInputStream fis = new FileInputStream(file);

            byte[] dataBytes = new byte[BUFFER];

            int nread = 0;
            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
            fis.close();
            byte[] mdbytes = md.digest();

            hexString = new StringBuffer();
            for (int i = 0; i < mdbytes.length; i++) {
                hexString.append(Integer.toHexString((0xFF & mdbytes[i])));
            }

            return hexString.toString();

        } catch (NoSuchAlgorithmException | HasherException e) {
            throw new HasherException("Unsuppored Hash Algorithm.", e);
        }
    }
	
	/**
	 * Validates input for supported hash algorithms. <br/>
	 * Currently supported algorithms are: <br/>
	 *     MD2 <br/>
	 *     MD5 <br/>
	 *     SHA-1 <br/>
	 *     SHA-256 <br/>
	 *     SHA-384 <br/>
	 *     SHA-512 <br/>
	 * 
	 * @param input Input string for selected hash algorithm.
	 * @return Correctly formatted and valid hash algorithm string.
	 * @throws HashTypeException If no supported or valid algorithm was found..
	 */
	protected static String validateHashType(String input) throws HasherException {
	    
	    // all possible hashing algorithms
	    final String[] possibleHashAlgo = {"MD2", "MD5", "SHA-1", "SHA-256", "SHA-384", "SHA-512"};
	    
	    String cleanStr = input.replaceAll("[^0-9a-zA-Z]+", "");
	    
	    for (String algo : possibleHashAlgo) {
	        if (cleanStr.equalsIgnoreCase(algo.replaceAll("[^0-9a-zA-Z]+", ""))) {
	            return algo;
	        }
	    }
	    // Throw HashTypeException if no supported or valid algorithm is found..
	    throw new HasherException("Unsuppored Hash Algorithm.");
	}
}

