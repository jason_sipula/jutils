/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.io;

/**
 * File Filter (WIP) - Requires 3rd Party Library
 * 
 * @author Jason Sipula
 *
 */
public class FFilter {
	
}

//import java.io.File;
//import java.io.FileFilter;

//public class FFilter implements FileFilter {
//	public boolean accept(File pathname, String filter) {
//		if (pathname.getName().endsWith(filter)) {
//			return true;
//		}
//		return false;
//	}
//}
