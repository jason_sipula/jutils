/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.io;

import java.io.File;

/**
 * Moving file utilities.
 * 
 * @author Jason Sipula
 *
 */
public class FileMover {
	
	/**
	 * Move specified file to another location.
	 * 
	 * @param fileToBeMoved String value of path + filename input.
	 * @param directoryToMoveTo String value of path + filename output. 
	 * @return true if success.
	 */
	public boolean fileMover(String fileToBeMoved, String directoryToMoveTo) {
		File file = new File(fileToBeMoved);
		File dir = new File(directoryToMoveTo);
		boolean success = file.renameTo(new File(dir, file.getName()));
		return success;
		
	}
}