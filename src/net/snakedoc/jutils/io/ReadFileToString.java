/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.io;

import java.io.File;
import java.io.IOException;
import java.net.URI;

/**
 * Reading files to string utilities.
 * 
 * @author Jason Sipula
 *
 */
public class ReadFileToString {
	
	/**
	 * Read from file and return sring value.
	 * 
	 * @param strFile String of path + filename to file.
	 * @return String value of file.
	 */
	public String readFromFile(String strFile) {
       File file = new File(strFile);
       URI uri = file.toURI();
       byte[] bytes = null;
       try {
          bytes = java.nio.file.Files.readAllBytes(
        		  java.nio.file.Paths.get(uri));
       } catch(IOException e) {
    	   e.printStackTrace(); 
    	   return "ERROR loading file " + strFile;
       }
       return new String(bytes);
    }
}
