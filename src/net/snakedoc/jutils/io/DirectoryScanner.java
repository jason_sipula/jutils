/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Scanning directory utilities
 * 
 * @author Jason Sipula
 *
 */
public class DirectoryScanner {
	
	/**
	 * Get List of directories.
	 * 
	 * @param dir String value of directory to scan.
	 * @return List of valid sub-directories.
	 */
	public List<File> getList(String dir) {
		List<File> realFiles = new ArrayList<File>();
		File[] files = new File(dir).listFiles();
		for (int i = 0; i < files.length; i++) {
			try {
				if (files[i].getAbsolutePath().equalsIgnoreCase(files[i].getCanonicalPath())) {
					realFiles.add(files[i]);
				}
			} catch (IOException | SecurityException e1) {
				// not a real file, so continue iteration
				continue;
			}
		}
		return realFiles;
	}
}
