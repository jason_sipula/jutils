/*******************************************************************************
 *  Copyright 2013 Jason Sipula                                                *
 *                                                                             *
 *  Licensed under the Apache License, Version 2.0 (the "License");            *
 *  you may not use this file except in compliance with the License.           *
 *  You may obtain a copy of the License at                                    *
 *                                                                             *
 *      http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                             *
 *  Unless required by applicable law or agreed to in writing, software        *
 *  distributed under the License is distributed on an "AS IS" BASIS,          *
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
 *  See the License for the specific language governing permissions and        *
 *  limitations under the License.                                             *
 *******************************************************************************/

package net.snakedoc.jutils.xml;

import java.io.File;   
import java.io.IOException;
import java.io.StringReader;  
  
import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;   
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;  
import javax.xml.transform.dom.DOMSource;  
import javax.xml.transform.stream.StreamResult;  
  
import org.w3c.dom.Document;  
import org.xml.sax.InputSource;  
import org.xml.sax.SAXException;

/**
 * Working with XML tools
 * 
 * @author Jason Sipula
 *
 */
public class TransformXML {
	
	/**
	 * Converts a string to XML document
	 * and writes to a file by implementing
	 * transformXML method.
	 * 
	 * @param xmlString String value to convert.
	 * @param saveDirectory Directory to save file in.
	 * @param fileName name of file.
	 * @return true if success.
	 */
	public boolean convertStringtoXMLDoc(String xmlString, String saveDirectory, String fileName) {
		boolean success = false;  	
        // parse String to XML
        try {
        	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        	DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			transformXML(doc, saveDirectory, fileName, 4, "UTF-8");
			success = true;
		} catch (SAXException e) {
			e.printStackTrace();
			success = false;
		} catch (IOException e) {
			e.printStackTrace();
			success = false;
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			success = false;
		}
        return success;
	}
	
	/**
	 * Transform document tree into correctly formatted XML and save to file.
	 * 
	 * @param doc Document to convert to XML.
	 * @param saveLoc Directory/Location to save XML file in.
	 * @param fileName Name of file.
	 * @param indentSpaces The number of spaces to indent. Recommended 4.
	 * @param encoding The encoding to use on the XML document. Recommended "UTF-8".
	 * @return true if success.
	 */
	public boolean transformXML(Document doc, String saveLoc, 
						String fileName, int indentSpaces, String encoding) {
     	
     	//set up a transformer
     	TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = null;
        
    	try {
    		trans = transfac.newTransformer();
    	} catch (TransformerConfigurationException e) {
    		e.printStackTrace();
    	}
    	
    	trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
    	trans.setOutputProperty(OutputKeys.INDENT, "yes");
    	trans.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(indentSpaces));
    	trans.setOutputProperty(OutputKeys.ENCODING, encoding);
    	doc.setXmlStandalone(true);
		
    	//create string from xml tree
    	StreamResult result = new StreamResult(new File(saveLoc + fileName));
    	DOMSource source = new DOMSource(doc);
        	
    	// perform transformation of String to XML and output to file
    	try {
    		trans.transform(source, result);
    		return true;
		} catch(Exception e) {
        	e.printStackTrace();
        	return false;
        }
	}
}