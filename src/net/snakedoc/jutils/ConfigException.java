package net.snakedoc.jutils;

public class ConfigException extends Exception {
    
    /**
     * generated serialVersion UID
     */
    private static final long serialVersionUID = 1082775592090609311L;

    public ConfigException () {} // empty constructor

    public ConfigException (String message) {
        super (message);
    }

    public ConfigException (Throwable cause) {
        super (cause);
    }

    public ConfigException (String message, Throwable cause) {
        super (message, cause);
    }
}